class site_logcheck {

  include logcheck::install

  class { 'logcheck::configure':
    email                   => hiera('logcheck::email', 'collective@riseup.net'),
    intro                   => '0',
    sortuniq                => '1',
    support_cracking_ignore => '1'
  }

  class { 'logcheck::logfiles':
    log_files => hiera('logcheck::log_files')
  }
}
